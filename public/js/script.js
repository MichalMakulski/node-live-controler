var ajax = AJAX(); 

function AJAX() {
	var xmlhttp = new XMLHttpRequest(); 

	function get (src, query) {  
		query = query || '';
		return new Promise(function(resolve, reject) {
			xmlhttp.onreadystatechange = function () {
				if (this.readyState === 4 && this.status === 200) {
					resolve(this.responseText);
				} 
			};
			xmlhttp.open("GET", src + query, true);
			xmlhttp.send();
		});
	}

	return {
		get: get
	}
}

function sendMove (ev) {
    var target = ev.target;
    if (target.nodeName === 'BUTTON') {
        var direction = target.textContent.toLowerCase();
        ajax.get('/move', '?direction=' + direction).then(moveSquare);
    }
}

function moveSquare (direction) {
    console.log(direction);
    var square = document.querySelector('.square');
    var sqTop = square.getBoundingClientRect().top;
    var sqLeft = square.getBoundingClientRect().left;
    
    if (direction === 'right') {
        square.style.left = (sqLeft + 50) + 'px'
    }
    if (direction === 'left') {
        square.style.left = (sqLeft - 50) + 'px'
    }
    if (direction === 'up') {
        square.style.top = (sqTop - 50) + 'px'
    }
    if (direction === 'down') {
        square.style.top = (sqTop + 50) + 'px'
    }
}

function log(data) {
    console.log(data);
}

document.addEventListener('click', sendMove);