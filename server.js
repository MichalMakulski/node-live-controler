var express = require('express');
var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
   res.sendFile(__dirname + '/views/index.html'); 
});

app.get('/move', function(req, res) {
   res.send(req.query.direction); 
});

app.listen(1185, function () {
    console.log('Server running...');
});